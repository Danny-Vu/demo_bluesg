import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late GoogleMapController _mapController;

  // final List<LocationEntity> _listDumData = [
  //   LocationEntity(lat: 1.273125 , lng: 103.82106),
  //   LocationEntity(lat: 1.31187, lng: 103.79699),
  //   LocationEntity(lat: 1.351144, lng: 103.928663),
  //   LocationEntity(lat: 1.28803, lng: 103.821693),
  // ];

  Map<String, Marker> _marker = {};

  final GetListLocationsProvider _getListLocationsProvider = GetListLocationsProvider();
  List<LocationEntity> _listLocations = [];

  @override
  void initState() {
    super.initState();
    _handleData();
  }

  void _handleData() async {
    final DataResponse data = await _getListLocationsProvider.getListLocation();
    List<List<double>> coordinate = [];
    data.result?.forEach((element) {
      if (element.locationResponse?.coordinate?.isNotEmpty == true) {
        coordinate.add(element.locationResponse?.coordinate ?? []);
      }
    });

    for (var element in coordinate) {
      _listLocations.add(LocationEntity(lat: element.last, lng: element.first));
    }
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: const CameraPosition(
        target: LatLng(1.3837389103185143, 103.83828355485156),
        zoom: 11,
      ),
      onMapCreated: (controller) {
        _mapController = controller;
        for (var i = 0; i< _listLocations.length ; i++) {
          _addMarker("$i", _listLocations[i]);
        }

      },
      markers: _marker.values.toSet(),
    );
  }

  _addMarker(String id, LocationEntity location) {
    var marker = Marker(
      markerId: MarkerId(id),
      position: LatLng(location.lat, location.lng),
     
    );
    _marker[id] = marker;
    setState(() {
    });
  }
}

class LocationEntity {
  final double lat;
  final double lng;

  LocationEntity({this.lat = 0.0, this.lng = 0.0});
}

class LocationResponse {
  final String? type;
  final List<double>? coordinate;

  LocationResponse({this.type, this.coordinate});
  factory LocationResponse.fromJson(Map<String, dynamic> json) {
    var data = json['coordinates'].toString();
    List<String> stringValues = data.replaceAll(RegExp(r'[\[\] ]'), '').split(',');
    List<double> doubleValues = stringValues.map((stringValue) => double.parse(stringValue)).toList();
    return LocationResponse(type: json['type'].toString(),
      coordinate: doubleValues,
    );
  }
}

class PositionResponse {
  final LocationResponse? locationResponse;

  PositionResponse({this.locationResponse});

  factory PositionResponse.fromJson(Map<String, dynamic> json) {
    return PositionResponse(locationResponse: LocationResponse.fromJson(json['position']));
  }
}

class DataResponse {
  final List<PositionResponse>? result;

  DataResponse({this.result});

  factory DataResponse.fromJson(Map<String, dynamic> json) {
    var results = json['result'];
    List<PositionResponse>? list;
    if (results != null && results is List) {
      list = results.map((e) => PositionResponse.fromJson(e)).toList();
    }
    return DataResponse(result: list);
  }
}

abstract class IGetListLocations {
  Future<DataResponse> getListLocation();
}

class GetListLocationsProvider extends IGetListLocations {

  @override
  Future<DataResponse> getListLocation() async {
    final jsonData = await rootBundle.loadString("assets/mock_data/data.json");
    final data = DataResponse.fromJson(json.decode(jsonData));
    return data;
  }
}

